﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class BaseClient : IDisposable
    {
        protected Uri serverUri;
        protected HttpClient httpClient;

        protected Uri getPath(String path)
        {
            return new Uri(serverUri, path);
        }

        public BaseClient(String serverUrl)
        {
            serverUri = new Uri(serverUrl);
            httpClient = new HttpClient();
            httpClient.BaseAddress = serverUri;
        }

        protected HttpResponseMessage Get(String path)
        {
            return httpClient.GetAsync(path).Result;
        }

        protected HttpResponseMessage Post(String path, String payload)
        {
            HttpResponseMessage response;
            StringContent content = new StringContent(payload, Encoding.UTF8);
            response = httpClient.PostAsync(path, content).Result;
            return response;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}
