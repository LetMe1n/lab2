﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Client : BaseClient
    {
        JsonSerializer<Output> outputSerializer;
        JsonSerializer<Input> inputSerializer;

        public Client(String serverUrl) : base(serverUrl)
        {
            outputSerializer = new JsonSerializer<Output>();
            inputSerializer = new JsonSerializer<Input>();
        }

        public bool ping()
        {
            bool isSuccess;
            try
            {
                using (HttpResponseMessage response = Get("Ping"))
                {
                    isSuccess = (response.StatusCode == HttpStatusCode.OK);
                }
            } catch (AggregateException ae)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public Input GetInputData()
        {
            Input input;
            using (HttpResponseMessage response = Get("GetInputData"))
            {
                Stream dataStream = response.Content.ReadAsStreamAsync().Result;
                input = inputSerializer.Deserialize(dataStream); ;
                dataStream.Close();
            }
            return input;
        }

        public void WriteAnswer(Output output)
        {
            MemoryStream stream = outputSerializer.Serialize(output);
            String payload = new string(Encoding.UTF8.GetChars(stream.ToArray()));
            Post("WriteAnswer", payload);
        }
    }
}
