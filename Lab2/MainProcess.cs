﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class MainProcess
    {
        public void run()
        {
            String port = Console.ReadLine();
            using ( Client client = new Client("http://127.0.0.1:" + port + "/") )
            {
                if (client.ping())
                {
                    Input input = client.GetInputData();
                    Output output = processing(input);
                    client.WriteAnswer(output);
                }
            }            
        }

        // Расчет вывода
        protected Output processing(Input input)
        {
            Output output = new Output();
            output.SumResult = input.Sums.Select(x => x * input.K).Sum();
            output.MulResult = input.Muls.Aggregate((x, next) => x * next);
            var decimalMuls = input.Muls.Select(x => (decimal)x).ToArray();
            output.SortedInputs = decimalMuls.Concat(input.Sums).OrderBy(x => x).ToArray();

            for (int i = 0; i < output.SortedInputs.Count(); i++)
            {
                if (output.SortedInputs[i] - (int)output.SortedInputs[i] == 0)
                    output.SortedInputs[i] *= 1.0m;
            }
            return output;
        }
    }
}
